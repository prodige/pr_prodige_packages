FROM debian:buster

WORKDIR /opt
RUN mkdir /opt/packages && mkdir /packages

# checkinstall
RUN mkdir /opt/checkinstall
RUN apt update && \
    apt install wget -y

RUN cd /opt/checkinstall && wget http://mirror.alkante.al/debian/ftp.fr.debian.org/debian/pool/main/c/checkinstall/checkinstall_1.6.2-4_amd64.deb -O checkinstall_1.6.2-4_amd64.deb
RUN cd /opt/checkinstall && apt install -y ./checkinstall_1.6.2-4_amd64.deb

# libecw
RUN mkdir /opt/libecw
RUN apt update && \
    apt install wget zip patch -y

RUN cd /opt/libecw && wget https://github.com/bogind/libecwj2-3.3/raw/master/libecwj2-3.3-2006-09-06.zip -O libecwj2-3.3-2006-09-06.zip
RUN cd /opt/libecw && unzip libecwj2-3.3-2006-09-06.zip
COPY ./libecw/libecwj2-3.3.patch /opt/libecw
COPY ./libecw/libecwj2-3.3-msvc90-fixes.patch /opt/libecw
RUN cd /opt/libecw && patch -l -p0 < libecwj2-3.3.patch
RUN cd /opt/libecw && patch -l -p0 < libecwj2-3.3-msvc90-fixes.patch
RUN cd /opt/libecw/libecwj2-3.3 &&\
    ./configure --prefix=/usr/local &&\
    make &&\
    echo "ECW libraries" > description-pak &&\
    checkinstall -D --install=no --pkgname=libecwj2 --pkgversion=3.3 --arch=amd64 -y --pkgrelease=1buster --maintainer=debian@alkante.com --replaces none --pkgaltsource none make install
RUN cd /opt/libecw/libecwj2-3.3 && apt install -y ./libecwj2_3.3-1buster_amd64.deb
RUN mv /opt/libecw/libecwj2-3.3/libecwj2_3.3-1buster_amd64.deb /opt/packages/

# Geos
## Dep compil
RUN apt update && \
    apt-get install -y libcurl4-gnutls-dev libgeos-dev libxml2-dev libpng-dev libjpeg-dev libgif-dev python-dev libsqlite3-dev libxerces-c-dev libpq-dev libkml-dev libfreexl-dev libexpat1-dev libopenjp2-7-dev libpcre3-dev
## Dep cli
RUN apt update && \
    apt-get install -y curl libcurl4 libcurl3-gnutls libgeos-c1v5 libgif7 libjpeg62-turbo libpng16-16 python libsqlite3-0 libxerces-c3.2 libpq5 libxml2 libkmlbase1 libfreexl1 libexpat1 libopenjp2-7 libpcre3 gdal-abi-2-4-0 python-numpy
RUN geos-config --version

# Gdal
RUN mkdir /opt/gdal
RUN apt update && \
    apt-get install -y build-essential fakeroot devscripts strace dh-make lsb-release libtool
RUN echo 'deb-src http://ftp.fr.debian.org/debian buster main contrib non-free' >> /etc/apt/sources.list &&\
    apt-get update && \
    apt-get build-dep gdal libgdal-dev -y
RUN cd /opt/gdal && wget -q http://cdn-fastly.deb.debian.org/debian/pool/main/g/gdal/gdal_2.4.0+dfsg-1.dsc -O gdal_2.4.0+dfsg-1.dsc
RUN cd /opt/gdal && wget -q http://deb.debian.org/debian/pool/main/g/gdal/gdal_2.4.0+dfsg.orig.tar.xz -O gdal_2.4.0+dfsg.orig.tar.xz
RUN cd /opt/gdal && wget -q http://deb.debian.org/debian/pool/main/g/gdal/gdal_2.4.0+dfsg-1.debian.tar.xz -O gdal_2.4.0+dfsg-1.debian.tar.xz
RUN cd /opt/gdal && tar -xf gdal_2.4.0+dfsg-1.debian.tar.xz
RUN cd /opt/gdal && tar -xf gdal_2.4.0+dfsg.orig.tar.xz
RUN cd /opt/gdal && cp -a debian gdal-2.4.0/
RUN cd /opt/gdal/gdal-2.4.0/ && sed -i 's/--with-ecw=no/--with-ecw=yes/' debian/rules
RUN cd /opt/gdal/gdal-2.4.0/ && echo 'libNCSEcw 0 libecwj2\n\
libNCSEcwC 0 libecwj2\n\
libNCSCnet 0 libecwj2\n\
libNCSUtil 0 libecwj2' > debian/shlibs.local
RUN cd /opt/gdal/gdal-2.4.0/ && dpkg-buildpackage -us -uc -d
RUN cp /opt/gdal/libgdal20_2.4.0+dfsg-1_amd64.deb /opt/gdal/gdal-bin_2.4.0+dfsg-1_amd64.deb /opt/gdal/gdal-data_2.4.0+dfsg-1_all.deb /opt/gdal/python3-gdal_2.4.0+dfsg-1_amd64.deb /opt/packages/

# Copy result in volume /opt/packages
RUN chmod 644 /opt/packages/*
CMD cp /opt/packages/* /packages/
