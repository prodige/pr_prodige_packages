# Prodige packages
Prodige utilise une version de gdal avec l'option ecw. Il est donc nécessaire de rebuild le package gdal de debian avec cette extension.

List des bundles prodige :
- ppr_prodige_admincarto
- ppr_prodige_catalogue
- ppr_prodige_datacarto
- ppr_prodige_mapserver_api
- ppr_prodige_visuliseur

Source ecw : https://trac.osgeo.org/gdal/wiki/ECW

## Build
```bash
docker build -t pr_prodige_packages .
```
## Get packages
```bash
docker run --rm -v ./packages:/packages pr_prodige_packages
```
Vous trouvez les 4 paquets debian dans le dossier "packages":
- gdal-bin
- gdal-data
- libecwj2
- ligdal20
- python3-gdal
